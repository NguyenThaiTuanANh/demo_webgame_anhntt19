// Phần HTML -> tạo thành phần cha, bọc layout grid ( đặt ở phần html )
// Phần xử lí JavaScript
// Lấy phần tử cha bằng id với mục đích là sẽ render phần grid trong thằng cha này với id = "sample-wall-container"
// với matrix 3 x 3 thì sẽ set i <= 9, 4 x 4 thì sẽ set i <= 16 ....
var listSampleWall = new Array();
var listResult = new Array();
window.onload = function() {
    renderSampleWall()
    renderWall()
    const container = document.getElementById("grid-gameboard");
    Array.from(container.children).forEach((element) => {
        listResult.push(element.id.replace("grid-item-wall-", ""));
    });
    console.log(listResult)
    var newsecond = 10,
        displays = document.querySelector('#time');
    startTimer(newsecond, displays);
}
const renderSampleWall = () => {
    const containerSampleWall = document.getElementById("grid-container");

    // matrix 3 x 3 number
    for (let i = 1; i <= 9; i++) {
        const div = renderItemSampleWallModel(i);
        containerSampleWall.appendChild(div);
    }
};

const renderItemSampleWallModel = (index) => {
    let div = document.createElement("div");

    const id = `grid-item-sample-wall-${index}`;
    const className = "grid-item";
    const numberRandom = Math.floor(Math.random() * 10);
    // add id, class, content, style
    div.id = id;
    div.className = className;
    div.textContent = "";
    // div.onclick = () => handleClick(id)
    div.style.visibility = numberRandom % 2 == 0 ? "hidden" : "none";
    if (numberRandom % 2 !== 0) {
        listSampleWall.push(index + "");
    }
    return div;
};


const renderWall = () => {
    const containerWall = document.getElementById("grid-gameboard");

    // matrix 3 x 3 number
    for (let i = 1; i <= 9; i++) {
        const divGameboard = renderItemWallModel(i);
        containerWall.appendChild(divGameboard);
    }
};

const renderItemWallModel = (index) => {
    let divGameboard = document.createElement("divGameboard");
    const id = `grid-item-wall-${index}`;
    const className = "grid-item-wall";
    // add id, class, content, style
    divGameboard.id = id;
    divGameboard.className = className;
    divGameboard.onclick = () => handleClick(id)
        // divGameboard.style.visibility = 'hidden'

    return divGameboard;

};
const handleClick = (id) => {
        // alert(id)
        let itemClick = new Array();

        const Play = document.getElementById(id);
        Play.style.visibility = "hidden"
        let index = listResult.indexOf(id.replace("grid-item-wall-", ""));
        if (index > -1) { // only splice array when item is found
            listResult.splice(index, 1); // 2nd parameter means remove one item only
        }
        //const container = document.getElementById("grid-gameboard");
        // Array.from(container.children).forEach((element) => {
        //     //console.log(element)
        //     if (element.isResult !== -1) {
        //         listResult.push(element.isResult);
        //     }
        // });
    }
    // renderWall()

function startTimer(duration, displays) {
    var timer = duration,
        seconds;
    setInterval(function() {
        seconds = parseInt(timer % 60, 10);
        seconds = seconds < 10 ? "0" + seconds : seconds;

        displays.textContent = seconds;

        if (--timer < 0) {
            isCheckResult();
            timer = duration;
        }
    }, 1000);
}

const isCheckResult = () => {
    console.log(JSON.stringify(listSampleWall.sort()))
    console.log(JSON.stringify(listResult.sort()))
    console.log()
    if (JSON.stringify(listSampleWall.sort()) == JSON.stringify(listResult.sort())) {
        alert("Chính xác!!!")
    } else {
        alert("Đáp án sai!!!")
    }

}